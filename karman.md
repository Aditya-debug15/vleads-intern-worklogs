# Introduction

  This document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.

# Work Logs

## Week 1 [2022-05-16 Mon] till [2022-05-20 Fri]

## Daily Logs

- Day1 [2022-05-16 Mon]: Introductory Meet assigned projects to work on , and discussed about the internship plan as we go through.
- Day2 [2022-05-17 Tue] : Given the documentation , and progress of the previous meet on the experiment , gone through the code base , understanding the code base.
- Day3 [2022-05-18 Wed] : Going through the codebase , understanding the code.
- Day4 [2022-05-19 Thu] : Going through the codebase , understanding the code , reviewing the changes to be done ( as given in suggestions and to/do section ) planning and choosing the experiment to work on.
- Day5 [2022-05-20 Fri] : Chosen experiment-10 to start with , started coding the experiment , analysed the changes , done basic coding for the experiment.

## Week 2 [2022-05-23 Mon] till [2022-05-27 Fri]

## Daily Logs

- Day1 [2022-05-23 Mon]: Done with the re-structuring the given code , made it modular and refined variable namings to convey more sense , There were 4 test cases , for which the given codebase had to be extended , planned for covering the same.
- Day2 [2022-05-24 Tue] : Test Case 1 & 2 done .
- Day3 [2022-05-25 Wed] : Test Case 3 & 4 done .
- Day4 [2022-05-26 Thu] : Bug Hunting , and display customisations ~ improved styling and display of the experiment components , fixed bugs .
- Day5 [2022-05-27 Fri] : Final touches , covering the experiment , adding descriptive error messages and fixing bugs , testing against edge cases ~ Code tested and uploaded on the git hub repository.

# Week Progress

| S.No | Overall Experiment Status | Previous Week Milestone | Next Week Milestone | Issues|
| :---: | :---: | :---: |:---: |:---: |
| 1. |Started with experiment 10  |Reviewed and understood the existing codebase | Finish experiment 10 | Will have to make major changes in the existing codebase , to account for all the possible test cases ( for slope of line to cover all cases )|
| 2. |Experiment 10 completed |Started coding the experiment 10 |Finishing experiment 8 | Displaying error messages in experiment 10. According to suggestions have to make changes in the way the text is being rendered on the screen , in apt manner.|


 












